﻿using UnityEngine;
using System.Collections;

public class Loading : MonoBehaviour {
	
	private GameStats gameStats;
	public TextMesh levelText;
	public TextMesh enemyTanksText;
	
	// Use this for initialization
	void Start () {
		gameStats = (GameStats)FindObjectOfType(typeof(GameStats));
		levelText.text = "Level: " + gameStats.currentLevel;
		StartCoroutine(loadMap());
	}
	
	void update () {
		
	}
	
	IEnumerator loadMap() {
 		yield return new WaitForSeconds(3);
 		Application.LoadLevel(1);
	}
}

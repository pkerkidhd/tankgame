﻿using UnityEngine;
using System.Collections;

public class GameStats : MonoBehaviour {
	
	public int currentLevel = 1;
	public int lives = 3;
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {
	
	private GameStats gameStats;
	public string versionTxt = "Version:";
	public GUIText version;
	public GUIText credits;
	public GUIText level;
	public TextMesh winLose;
	
	public int win = 0;
	
	
	// Use this for initialization
	void Start () {
		gameStats = (GameStats)FindObjectOfType(typeof(GameStats));
		version.text = versionTxt;
		credits.text = "Made by: David Vo";
		level.text = "Level: " + gameStats.currentLevel;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown(KeyCode.Escape)) {
        	Application.Quit();
    	}
		
		StartCoroutine(winCon());
	}
	
	IEnumerator winCon() {
		Debug.Log(win);
		if (win == 1) {
			winLose.text = "You Win!";
			winLose.renderer.enabled = true;
			gameStats.currentLevel = 2;
			yield return new WaitForSeconds(2);
			Application.LoadLevel(0);
		} else if (win == 2) {
			winLose.text = "You Lose!";
			winLose.renderer.enabled = true;
			gameStats.currentLevel = 1;
			yield return new WaitForSeconds(2);
			Application.LoadLevel(0);
		}
	}
}

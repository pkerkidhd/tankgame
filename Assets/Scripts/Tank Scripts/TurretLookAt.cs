﻿using UnityEngine;
using System.Collections;

public class TurretLookAt : MonoBehaviour {
	
	private Vector3 worldPos;
	private float mouseX;
	private float mouseY;
	private float cameraDif;
	public GameObject turretGunIn;
	public GameObject turretGunOut;
	
	// Use this for initialization
	void Start () {
		cameraDif = camera.transform.position.y - turretGunOut.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		
		mouseX = Input.mousePosition.x;
		mouseY = Input.mousePosition.y;
		worldPos = camera.ScreenToWorldPoint(new Vector3(mouseX, mouseY, cameraDif));
 		Vector3 turretLookDirection =  new Vector3(worldPos.x,turretGunOut.transform.position.y, worldPos.z); // here instead of Y value you might put something which suites your game better
 		turretGunIn.transform.LookAt(turretLookDirection);
		turretGunOut.transform.LookAt(turretLookDirection);
	}
}

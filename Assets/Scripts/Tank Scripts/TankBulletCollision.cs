﻿using UnityEngine;
using System.Collections;

public class TankBulletCollision : MonoBehaviour {
	
	private HUD hud;
	float isBounce;

	// Use this for initialization
	void Start () {
		
		hud = (HUD)FindObjectOfType(typeof(HUD));
		isBounce = 0.0f;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isBounce > 2) {
			Destroy(this.gameObject);
		}
	}
	
	void OnCollisionEnter (Collision Target) {
		if (Target.gameObject.tag == "EnemyTank") {
			Destroy(this.gameObject);
			Destroy(Target.gameObject);
			hud.win = 1;
		} else if (Target.gameObject.tag == "PlayerTank") {
			Destroy(this.gameObject);
			Destroy(Target.gameObject);
			hud.win = 2;
		} else {
			transform.rotation *= Quaternion.AngleAxis( 180, transform.up );
			isBounce++;
		}
		
//		if (Target.gameObject.tag == "PlayerTank") {
//			Destroy(this.gameObject);
//			Destroy(Target.gameObject);
//		}
	}
}

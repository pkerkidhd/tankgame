﻿using UnityEngine;
using System.Collections;

public class TankMovement : MonoBehaviour {
	
	public float Acc = 1.0f;
	public float Decc = -1.0f;
	public float Velo = 0.0f;
	public float maxSpeed = 2.0f;
	public float rotSpeed = 15.0f;
	public float jitControl = 0.02f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(new Vector3(0,0,Velo * Time.deltaTime));
		
		if (Velo <= jitControl && Velo >= -jitControl) {
			Velo = 0; //Set the velocity to 0, so there is no jittering of the tank when not moving.
		}
		// UP
		if (Input.GetKey(KeyCode.W)) {
			if (Velo <= maxSpeed) {
				Velo += Acc * Time.deltaTime;
			}
		} else if (Input.GetKey(KeyCode.S)) {
			if (Velo >= -maxSpeed) {
				Velo -= Acc * Time.deltaTime;
			}
		} else {
			if (Velo > 0) {
				Velo += Decc * Time.deltaTime;
			} else if (Velo < 0) {
				Velo -= Decc * Time.deltaTime;
			}
		}
		
		// LEFT
		if (Input.GetKey(KeyCode.A)) {
			if (Input.GetKey(KeyCode.S)) {
				transform.Rotate(new Vector3(0, rotSpeed * Time.deltaTime, 0));
			} else {
				transform.Rotate(new Vector3(0, -rotSpeed * Time.deltaTime, 0));
			}
		}
		
		// RIGHT
		if (Input.GetKey(KeyCode.D)) {
			if (Input.GetKey(KeyCode.S)) {
				transform.Rotate(new Vector3(0, -rotSpeed * Time.deltaTime, 0));
			} else {
				transform.Rotate(new Vector3(0, rotSpeed * Time.deltaTime, 0));
			}
		}
	}
	//END UPDATE
}

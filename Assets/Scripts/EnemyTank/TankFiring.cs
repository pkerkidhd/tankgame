﻿using UnityEngine;
using System.Collections;

public class TankFiring : MonoBehaviour {
	
	public GameObject playerTank;
	public Rigidbody projectile;
	public float speed = 10.0f;
	float lastFireTime;
	float shootDelay = 3.5f;
	float dist;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		dist = Vector3.Distance(playerTank.transform.position, transform.position);
		Vector3 relativePos = playerTank.transform.position - transform.position;
		Quaternion rotation = Quaternion.LookRotation(relativePos);
		transform.rotation = rotation;
		
		if (dist < 11 ) {
		if(Time.time > (lastFireTime + shootDelay)) {
				//antimateTurret.animation.Play("TankTurretMove");
				Rigidbody clone = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
				clone.velocity = transform.TransformDirection(new Vector3(0, 0, speed));
				lastFireTime = Time.time;
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class TankLookAt : MonoBehaviour {
	
	public GameObject playerTank;
	//float dist; 
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		//dist = Vector3.Distance(playerTank.transform.position, transform.position);
			Vector3 relativePos = playerTank.transform.position - transform.position;
			Quaternion rotation = Quaternion.LookRotation(relativePos);
			transform.rotation = rotation;
	
	}
}
